import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login'
import Inicio from '@/views/Inicio'
import Productos from '@/views/productos_component/Producto'
import Ventas from '@/views/ventas_component/Venta'
import Ingreso from '@/views/ingresos_component/Ingreso'

import Empresas from '@/views/empresas_component/Empresas'
import AdministradorEmpresa from '@/views/empresas_component/AdministradorEmpresa'

import Sucursales from '@/views/sucursales_component/Sucursales'
import AdministradorSucursal from '@/views/sucursales_component/AdministradorSucursal'


Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass : 'active',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
      meta : {rutaVisitante: true}
    },
    {
      path: '/inicio',
      name: 'inicio',
      component: Inicio,
      meta : {rutaAutenticada: true}
    },
    {
      path: '/productos',
      name: 'prodcutos',
      component: Productos,
      meta : {rutaAutenticada: true}
    },
    {
      path: '/empresas',
      name: 'empresas',
      component: Empresas,
      meta : {rutaAutenticada: true}
    },
    {

      path: '/ventas',
      name: 'ventas',
      component: Ventas,
      meta : {rutaAutenticada: true}
    },
    
    {
      path: '/ingresos',
      name: 'ingreso',
      component: Ingreso,
      meta : {rutaAutenticada: true}
    },
    {
        path: '/empresa/administrador',
      name: 'administrador-empresa',
      component: AdministradorEmpresa,
      meta : {rutaAutenticada: true}
    },
    {
      path: '/sucursales',
      name: 'sucursales',
      component: Sucursales,
      meta : {rutaAutenticada: true}
    },
    {
      path: '/sucursales/admin',
      name: 'administrador-sucursal',
      component: AdministradorSucursal,
      meta : {rutaAutenticada: true}
    }
  ]
})
