<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

/**
 * Rutas de autenticación generadas por Laravel Auth
 */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/**
 * Rutas para la gestion de las empresas
 */
Route::resource('/empresas', 'EmpresaController');

/**
 * Rutas para la gestion de las facturas
 */
Route::resource('/facturas', 'FacturaController');
Route::get('/facturas/confirmar', 'FacturaController@confirmarFactura');

/**
 * Rutas para la gestion del ingreso de productos a bodega
 */
Route::resource('/ingresos', 'IngresoProductoController');

/**
 * Rutas para la gestion de los tipos de documentos almacenados
 */
Route::resource('/tipo-documento', 'TipoDocumentoController');

/**
 * Ruta para la gestión de productos nuevos a bodega
 */
Route::resource('/productos', 'ProductoController');
Route::get('/productos/{criterio}/{palabra}', 'ProductoController@buscar');


/**
 * Rutas para la gestion de los roles
 */
Route::resource('/roles', 'RolController');

/**
 * Rutas para la gestion de las sucursales
 */
Route::resource('/sucursales', 'SucursalController');
Route::get('/sucursal/{criterio}/{palabra}', 'SucursalController@buscar');

/**
 * Rutas para la gestion de los usuarios
 */
Route::get('/usuarios/ver/{id_empresa}', 'UserController@index');
Route::get('/usuarios/ver/{id_empresa}/{user_id}', 'UserController@show');
Route::get('/usuarios/{criterio}/{palabra}', 'UserController@buscar');
Route::post('/usuarios/{id_empresa}', 'UserController@store');
Route::put('/usuarios/{id_empresa}', 'UserController@update');
Route::delete('/usuarios/{id_empresa}/{id_usuario}', 'UserController@destroy');

/**
 * Rutas para la gestion de las facturas de ventas
 */
Route::resource('/factura', 'FacturaController');

Route::get('/sucursal/{criterio}/{palabra}', 'SucursalController@buscar');

Route::get('/empresa/{criterio}/{palabra}', 'EmpresaController@buscar');

//Ruta para provar métodos :v
Route::get('/pruebas/{parametro}', 'FacturaController@facturaBorrador');

//ruta para rescatar id usuario, rol y id de la sucursal
Route::get('usuario/sucursal/{id_usuario}', 'UserController@getSucursal');