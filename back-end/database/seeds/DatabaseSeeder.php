<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Agregamos todas las migraciones para correrlas con php artisan db:seed
        $this->call(RolSeeder::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SucursalSeeder::class);
        $this->call(FacturaSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(IngresoProductoSeeder::class);
    }
}
