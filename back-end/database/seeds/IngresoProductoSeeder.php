<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IngresoProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 50 ingresos de productos en ls BD
        for ($i = 0; $i < 50; $i++) {
            DB::table('ingreso_productos')->insert([
                'codigo_ingreso' => $i,
                'observaciones_ingreso' => 'Observación Semilla' . $i * rand(100, 99999),
                'user_id_i' => rand(0, 49),
                'ingreso_empresa' => rand(1,25)
            ]);
        }
    }
}
