<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 25 registros de empresas en la BD
        for($i = 0; $i < 25; $i++){
            DB::table('empresas')->insert([
                'nit_empresa' => rand(10000000, 1094999999),
                'nombre_empresa'=> 'Empresa Semilla'.$i,
            ]);
        }
    }
}
