<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 100 productos en la BD
        for ($i = 0; $i < 100; $i++) {
            $precio = rand(20000, 1000000);
            DB::table('productos')->insert([
                'nombre_producto' => 'Producto Semilla ' . $i,
                'descripcion_producto' => 'Descripcion Semilla ' . $i,
                'stock_producto' => rand(100, 999),
                'IVA' => 19,
                'precio_compra' =>  $precio,
                'precio_venta' => $precio+($precio*0.19) + ($precio*0.2),
                'sucursal_producto' => rand(1, 100),
                'empresa_producto' => rand(1,25)
            ]);
        }
    }
}
