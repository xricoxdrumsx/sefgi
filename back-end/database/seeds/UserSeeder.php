<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 50 usuarios en la BD
        for ($i = 0; $i < 50; $i++) {
            DB::table('users')->insert([
                'user_id' => $i,
                'name' => 'Usuario Semilla' . $i,
                'lastname' => 'Apellido Semilla' . $i,
                'email' => 'correosemilla' . $i . '@mail.com',
                'password' =>  Hash::make('password'),
                'document_type' => rand(1, 3),
                'user_phone' =>  rand(7470000, 7479999),
                'user_role' => rand(1, 5),
                'empresa_usuario' => rand(1 ,25)
            ]);
        }
    }
}
