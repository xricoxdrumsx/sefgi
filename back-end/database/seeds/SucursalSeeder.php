<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 100 sucursales en la BD
        for ($i = 0; $i < 100; $i++) {
            DB::table('sucursales')->insert([
                'nit_sucursal' => 'nit-'.rand(1000, 9999),
                'nombre_sucursal' => 'Nombre Semilla' . $i,
                'sucursal_empresa' => rand(1, 25)
            ]);
        }
    }

}
