<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 50 registros de facturas en la BD
        for ($i = 0; $i < 50; $i++){
            DB::table('facturas')->insert([
                'codigo_factura' => $i,
                'codigo_producto' => rand(1, 100),
                'total_venta' => 0,
                'cedula_cliente' => rand(7000000, 2499999),
                'nombre_cliente' => 'Cliente Semilla',
                'telefono_cliente' => rand(7370000, 7479999),
                'user_id_f' => rand(0, 49),
                'empresa_factura' => rand(1,25)
            ]);
        }
    }

}
