<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tipodocumentoseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de tres tipos de documentos en la BD
        DB::table('tipo_documentos')->insert(['nombre_docuemnto' => 'CC']);
        DB::table('tipo_documentos')->insert(['nombre_docuemnto' => 'CE']);
        DB::table('tipo_documentos')->insert(['nombre_docuemnto' => 'Pas']);
    }
}
