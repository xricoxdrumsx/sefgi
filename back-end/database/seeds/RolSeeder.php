<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insercion de 5 roles en la BD
        DB::table('roles')->insert(['nombre_rol' => 'super']);
        DB::table('roles')->insert(['nombre_rol' => 'admin']);
        DB::table('roles')->insert(['nombre_rol' => 'sucursal']);
        DB::table('roles')->insert(['nombre_rol' => 'ventas']);
        DB::table('roles')->insert(['nombre_rol' => 'bodega']);
    }
}
