<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngresoProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingreso_productos', function (Blueprint $table) {
            //Creamos los datos de nuestra base de datos
            $table->bigInteger('codigo_ingreso')->primary();
            $table->string('observaciones_ingreso', 256)->nullable();
            $table->string('user_id_i');
            $table->boolean('estado_ingreso')->default(1);
            $table->bigInteger('ingreso_empresa');
            $table->timestamps();

            //Llave foránea para relacionar el ingreso de productos con el usuario que lo realizó
            $table->foreign('user_id_i')->references('user_id')->on('users');
            $table->foreign('ingreso_empresa')->references('id_empresa')->on('empresas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingreso_productos');
    }
}
