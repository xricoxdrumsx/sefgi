<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            //Asignamos los datos de la tabla sucursales
            $table->bigInteger('id_sucursal')->autoIncrement();
            $table->string('nit_sucursal', 25);
            $table->string('nombre_sucursal', 45);
            $table->boolean('estado_sucursal')->default(1);
            $table->bigInteger('sucursal_empresa');
            $table->timestamps();

            //Llave foranea para la relación entre empresa y sucursales
            $table->foreign('sucursal_empresa')->references('id_empresa')->on('empresas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursals');
    }
}
