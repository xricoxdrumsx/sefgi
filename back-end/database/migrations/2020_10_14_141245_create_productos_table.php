<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            //Creamos nuestros propios campos para la tabla en la base de datos
            $table->bigInteger('codigo_producto')->autoIncrement();
            $table->string('nombre_producto', 45);
            $table->string('descripcion_producto', 256)->nullable();
            $table->integer('stock_producto');
            $table->float('IVA')->nullable();
            $table->integer('precio_compra');
            $table->integer('precio_venta')->nullable();
            $table->bigInteger('sucursal_producto');
            $table->boolean('estado_producto')->default(1);
            $table->bigInteger('empresa_producto');
            $table->timestamps();

            //Llave foranea para relacionar el producto con su respectiva sucursal
            $table->foreign('sucursal_producto')->references('id_sucursal')->on('sucursales');
            $table->foreign('empresa_producto')->references('id_empresa')->on('empresas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
