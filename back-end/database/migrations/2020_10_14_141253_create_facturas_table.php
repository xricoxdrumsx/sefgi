<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            //Asignamos nuestros campos en la tabla de las facturas
            $table->bigInteger('codigo_factura')->primary();
            $table->bigInteger('codigo_producto')->nullable();
            $table->bigInteger('total_venta')->nullable();
            $table->string('observacion_anulacion')->nullable();
            $table->string('cedula_cliente', 15)->nullable();
            $table->string('nombre_cliente', 50)->nullable();
            $table->integer('telefono_cliente')->nullable();
            $table->string('user_id_f', 25);
            //Estados: 0 = anulada, 1 = terminada, 2 = borrador, 3 = en cola
            $table->tinyInteger('estado_factura')->default(2);
            $table->bigInteger('empresa_factura');
            $table->timestamps();

            //Relaciones mediantes llave foráneas
            $table->foreign('user_id_f')->references('user_id')->on('users');
            $table->foreign('empresa_factura')->references('id_empresa')->on('empresas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
