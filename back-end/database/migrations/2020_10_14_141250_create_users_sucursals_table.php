<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_sucursales', function (Blueprint $table) {
            //Atributos que relacionan las tablas users con sucursales
            $table->bigInteger('codigo_sucursal_US');
            $table->string('user_id_US', 25);
            $table->timestamps();

            //Llaves foráneas para relacionar las tablas users y sucursales
            $table->foreign('codigo_sucursal_US')->references('id_sucursal')->on('sucursales');
            $table->foreign('user_id_US')->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_sucursals');
    }
}
