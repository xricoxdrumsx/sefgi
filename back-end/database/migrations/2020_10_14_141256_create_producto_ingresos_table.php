<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_ingresos', function (Blueprint $table) {
            //Declaramos nuestros atributos para la tabla puente entre produtos e ingresos
            $table->bigInteger('producto_ingreso_PI');
            $table->bigInteger('ingreso_producto_PI');
            $table->integer('stock_productos_i');   
            $table->timestamps();

            //Llaves foráneas para relacionar las tablas principales productos e ingresos
            $table->foreign('producto_ingreso_PI')->references('codigo_producto')->on('productos');
            $table->foreign('ingreso_producto_PI')->references('codigo_ingreso')->on('ingreso_productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_ingresos');
    }
}
