<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //Atributos usados por Laravel Auth por defecto, por este motivo se dejan en inglés
            $table->string('user_id', 25)->primary(); 
            $table->string('name', 45);
            $table->string('lastname', 45);
            $table->string('email', 60)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 256);
            $table->integer('user_phone');
            $table->boolean('user_state')->default(1);
            $table->tinyInteger('user_role');
            $table->tinyInteger('document_type');
            $table->bigInteger('empresa_usuario');
            $table->rememberToken();
            $table->timestamps();

            //Relación entre las llaver foráneas
            $table->foreign('user_role')->references('codigo_rol')->on('roles');
            $table->foreign('document_type')->references('codigo_documento')->on('tipo_documentos');
            $table->foreign('empresa_usuario')->references('id_empresa')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
