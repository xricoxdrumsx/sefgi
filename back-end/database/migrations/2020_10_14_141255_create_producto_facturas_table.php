<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_facturas', function (Blueprint $table) {
            //Tabla puente entre los productos y las facturas
            $table->bigInteger('codigo_producto_PF');
            $table->bigInteger('codigo_factura_PF');
            $table->integer('cantidad_producto');
            $table->integer('subtotal');
            $table->timestamps();

            //Llaves foráneas que relacionan los productos con las facturas
            $table->foreign('codigo_producto_PF')->references('codigo_producto')->on('productos');
            $table->foreign('codigo_factura_PF')->references('codigo_factura')->on('facturas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_facturas');
    }
}
