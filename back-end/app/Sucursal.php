<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    //Asignamos la tabla de la BD
    protected $table = 'sucursales';

    //Asignamos la llave primaria personalizada
    protected $primaryKey = 'id_sucursal';

    //Asignamos los campos que pueden ser editados
    protected $fillable = ['nit_sucursal', 'nombre_sucursal', 'estado_sucursal', 'empresa_sucursal', 'estado_sucursal'];

    /**
     * ____________________________________________
     *      Relaciones de Eloquent
     * ____________________________________________
     */

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }
}
