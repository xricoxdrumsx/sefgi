<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngresoProducto extends Model
{
    //Seleccionamos nuestra llave primaria personalizada
    protected $primaryKey = 'codigo_ingreso';

    //Agregamos los campos que pueden ser modificables en nuestra tabla
    protected $fillable = ['observaciones_ingreso', 'user_id_i'];
    
    /**
     * ____________________________________________
     *      Relaciones de Eloquent
     * ____________________________________________
     */

    public function empresa(){
        return $this->belongsTo('App\Empresa');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
