<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    //Asignamos nuestra llave primaria para hacer las debidas referencias
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     * Agregamos los atributos necesarios al modelo por defecto
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'user_phone', 'document_type', 'user_role', 'user_state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * ____________________________________________
     *      Relaciones de Eloquent
     * ____________________________________________
     */

    public function empresa(){
        return $this->belongsTo('App\Empresa');
    }

    public function facturas(){
        return $this->hasMany('App\Factura');
    }

    public function ingresoProductos(){
        return $this->hasMany('App\ingresoProducto');
    }
}
