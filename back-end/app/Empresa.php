<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Empresa extends Model
{
    //Asignamos nuestra llave primaria personalizada
    protected $primaryKey = 'id_empresa';

    //Asignamos los campos que pueden ser modigficados en nuestra tabla
    protected $fillable = ['nit_empresa', 'nombre_empresa', 'estado_empresa'];

/**
 * ____________________________________________
 *      Relaciones de Eloquent
 * ____________________________________________
 */
    public function facturas(){
        return $this->hasMany('App\Factura');
    }

    public function ingresoProductos(){
        return $this->hasMany('App\IngresoProducto');
    }

    public function productos(){
        return $this->hasMany('App\Producto');
    }

    public function sucursals(){
        return $this->hasMany('App\Sucursal');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
}
