<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    //Asignamos la llave primaria personalizada
    protected $primaryKey = 'codigo_factura';

    //Asignamos los campos que pueden ser modificados
    protected $fillable = [
        'codigo_producto', 'total_venta', 'estado_factura', 'observacion_anulacion', 'cedula_cliente',
        'nombre_cliente', 'telefono_cliente', 'user_id_f'
    ];

    /**
     * ____________________________________________
     *      Relaciones de Eloquent
     * ____________________________________________
     */

    public function empresa(){
        return $this->belongsTo('App\Empresa');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
