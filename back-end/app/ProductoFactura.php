<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoFactura extends Model
{
    //Declaramos los campos que pueden ser modificables de la tabla
    protected $fillable = [
        'codigo_producto_PF',
        'codigo_factura_PF',
        'cantidad_producto',
        'subtotal'
    ];
}
