<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    //Asignamos nuestra llave primaria codigo_documento
    protected $primaryKey = 'codigo_documento';

    //Asignamos como llenable, la propiedad nombre_documento de nuesta tabla
    protected $fillable = ['nombre_documento'];

}
