<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    //Asignamos nuestra tabla Roles
    protected $table = 'roles';

    //Asignamos nuestra llave primaria codigo_rol
    protected $primaryKey = 'codigo_rol';

    //Agregamos el atributo editable nombre_rol
    protected $fillable = ['nombre_rol'];
}
