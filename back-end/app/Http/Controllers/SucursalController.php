<?php

namespace App\Http\Controllers;

use App\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sucursal::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sucursal = new Sucursal();
        $sucursal->nit_sucursal = $request->nit_sucursal;
        $sucursal->nombre_sucursal = $request->nombre_sucursal;
        $sucursal->empresa_sucursal = $request->empresa_sucursal;
        $sucursal->save();
        return $sucursal;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function show($codigo_sucursal)
    {
        $sucursal = Sucursal::findOrFail($codigo_sucursal);
        return $sucursal;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $sucursal = Sucursal::findOrFail($request->codigo_sucursal);
        $sucursal->nit_sucursal = $request->nit_sucursal;
        $sucursal->nombre_sucursal = $request->nombre_sucursal;
        $sucursal->empresa_sucursal = $request->empresa_sucursal;
        $sucursal->save();
        return $sucursal;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sucursal  $sucursal
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_sucursal)
    {
        $sucursal = Sucursal::fidOrFail($codigo_sucursal);
        $sucursal->estado_sucursal = !$sucursal->estado_sucursal;
        $sucursal->save();
        return $sucursal;
    }
    
    public function buscar($criterio, $busqueda)
    {
        return DB::table('sucursales')->where($criterio,'like','%'.$busqueda.'%')->get();
        
    }
}
