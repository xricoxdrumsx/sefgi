<?php

namespace App\Http\Controllers;

use App\Factura;
use App\Producto;
use App\ProductoFactura;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Factura::all();
    }

    /**
     * Almacena una nueva factura en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //Iniciamos la transacción en la BD
            DB::beginTransaction();
            //Se obtiene el registro si hay de una factura en borrador
            $factura = $this->facturaBorrador($request->user_id_f);
            //Si el objeto no tiene elementos pasa directamente 
            if (!count($factura)) {
                //En primera instancia creamos y almacenamos los datos del ingreso que se va a realizar
                $factura = new Factura();
                $factura->codigo_factura = count(Factura::all());
                $factura->user_id_f = $request->user_id_f;
                $factura->empresa_factura = $request->empresa_factura;
                $factura->save();
                //Guardamos todos los cambios realizados
                DB::commit();
                return $factura;
            } else {
                //Deshacemos los cambios hechos
                DB::rollBack();
                //Se infroma que hay una factura en borrador y que debe ser manejada y se returna dicha factura
                return [
                    "mensaje" => "Tiene una factura pendiente, debe terminarla o anularla antes de crear una nueva",
                    "data" => $factura
                ];
            }
        } catch (Exception $e) {
            //En caso de tener algún error, deshacemos los cambios en la BD y retornamos el error
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show(Factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //Iniciamos transacción en la BD
        DB::beginTransaction();

        try {
            //Recuperamos la factura
            $temporal = Factura::find($request->codigo_factura);
            $temporal->codigo_producto = $request->codigo_producto;
            $temporal->cedula_cliente = $request->cedula_cliente;
            $temporal->nombre_cliente = $request->nombre_cliente;
            $temporal->telefono_cliente = $request->telefono_cliente;

            //En segundo lugar, vamos a manupular la tabla puente ente ingresos y productos
            $puente = new ProductoFactura();
            $puente->codigo_producto_PF = $request->codigo_producto;
            $puente->codigo_factura_PF = $request->codigo_factura;
            $puente->cantidad_producto = $request->cantidad_producto;

            //Actualizamos los atributos del producto
            $producto = Producto::find($request->codigo_producto);
            $producto->stock_producto -= $request->cantidad_producto;

            //Calculamos subtotal y guardamos la tabla puente
            $puente->subtotal = $producto->precio_venta * $request->cantidad_producto;
            $puente->save();

            //Guardamos la informacion del producto
            $producto->save();

            //Guardamos la facturas
            $temporal->save();

            return $temporal;
        } catch (Exception $e) {
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }

    public function facturaBorrador($parametro)
    {
        $factura = Factura::where('facturas.user_id_f', '=', $parametro)
            ->where('facturas.estado_factura', '=', 2)
            ->get();
        return $factura;
    }

    public function confirmarFactura($codigo_factura){
        $factura = Factura::findOrFail($codigo_factura);
        $factura->estado_factura = !$factura->estado_factura;
        $factura->save();
    }
}
