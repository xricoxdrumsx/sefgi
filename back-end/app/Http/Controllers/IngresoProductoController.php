<?php

namespace App\Http\Controllers;

use App\IngresoProducto;
use App\Producto;
use App\ProductoIngreso;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IngresoProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return IngresoProducto::all();
    }

    /**
     * Almacena un nuevo registro de un ingreso de producto, junto con su registro en la tabla puente y el aumento del respectivo stock
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //Iniciamos la transacción en la BD
            DB::beginTransaction();
            //En primera instancia creamos y almacenamos los datos del ingreso que se va a realizar
            $ingreso = new IngresoProducto();
            $ingreso->codigo_ingreso = count(IngresoProducto::all());
            $ingreso->observaciones_ingreso = $request->observaciones_ingreso;
            $ingreso->user_id_i = $request->user_id_i;
            $ingreso->ingres_empresa = $request->ingreso_empresa;
            $ingreso->save();

            //En segundo lugar, vamos a manupular la tabla puente ente ingresos y productos
            $puente = new ProductoIngreso();
            $puente->ingreso_producto_PI = IngresoProducto::find($ingreso->codigo_ingreso);
            $puente->producto_ingreso_PI = $request->codigo_producto;
            $puente->stock_productos = $request->stock_productos_i;
            $puente->save();

            //Actualizamos la cantidad de stock de dicho producto
            $producto = Producto::find($request->codigo_producto);
            $producto->stock += $request->stock_productos_i;
            $producto->save();

            //Guardamos todos los cambios realizados
            DB::commit();

        } catch (Exception $e) {
            //En caso de tener algún error, deshacemos los cambios en la BD y retornamos el error
            DB::rollBack();
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IngresoProducto  $ingresoProducto
     * @return \Illuminate\Http\Response
     */
    public function show($codigo_ingreso)
    {
        $ingreso = IngresoProducto::findOrFail($codigo_ingreso);
        return $ingreso;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IngresoProducto  $ingresoProducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ingreso = IngresoProducto::findOrFail($request->codigo_ingreso);
        $ingreso->observaciones_ingreso = $request->observaciones_ingreso;
        $ingreso->user_id_i = $request->user_id_i;
        $ingreso->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IngresoProducto  $ingresoProducto
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_producto)
    {
        $ingreso = IngresoProducto::findOrFail($codigo_producto);
        $ingreso->estado_ingreso = !$ingreso->estado_ingreso;
        $ingreso->save();
        return $ingreso;
    }

    private function existeProducto($codigo_producto)
    {
    }
}
