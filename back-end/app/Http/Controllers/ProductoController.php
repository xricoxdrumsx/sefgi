<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Producto::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto();

        $producto->nombre_producto = $request->nombre_producto;
        $producto->descripcion_producto = $request->descripcion_producto;
        $producto->stock_producto = $request->stock_producto;
        $producto->IVA = $request->IVA;
        $producto->precio_compra = $request->precio_compra;
        $producto->precio_venta = $request->precio_venta;
        $producto->sucursal_producto = $request->sucursal_producto;
        $producto->empresa_producto = $request->empresa_producto ;

        $producto->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($request)
    {
        return Producto::findOrFail($request);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $producto = Producto::findOrFail($request->codigo_producto);
        
        $producto->nombre_producto = $request->nombre_producto;
        $producto->descripcion_producto = $request->descripcion_producto;
        $producto->stock_producto = $request->stock_producto;
        $producto->IVA = $request->IVA;
        $producto->precio_compra = $request->precio_compra;
        $producto->precio_venta = $request->precio_venta;
        $producto->sucursal_producto = $request->sucursal_producto;
        $producto->empresa_producto = $request->empresa_producto ;

        $producto->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo)
    {
        $producto = Producto::findOrFail($codigo);
        $producto->estado_producto = !$producto->estado_producto;
        $producto->save();
    }

    /**
     * Busca los registros que existan en la BD y coincidan con los parámetros indicados
     */
    public function buscar($criterio, $busqueda)
    {
        $usuario = Producto::join('sucursales','productos.sucursal_producto','=','sucursales.id_sucursal')
        ->select('productos.codigo_producto','productos.nombre_producto','productos.descripcion_producto',
        'productos.stock_producto','productos.IVA','productos.precio_compra','productos.precio_venta', 'estado_producto',
        'sucursales.id_sucursal','sucursales.nit_sucursal', 'sucursales.nombre_sucursal')
        ->where('productos.'.$criterio,'like', '%'.$busqueda.'%')->get();
        return $usuario;
    }
}
