<?php

namespace App\Http\Controllers;

use App\TipoDocumento;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    /**
     * Retorna los tipos de docuemtos en la BD
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoDocumento::all();
    }

    /**
     * Almacena un registro de tipo de documento en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tDoc = new TipoDocumento();
        $tDoc->nombre_documento = $request->nombre_documento;
        return $tDoc;
    }

    /**
     * Muestra un tipo de documento específico
     *
     * @param  \App\TipoDocumento  $tipoDocumento
     * @return \Illuminate\Http\Response
     */
    public function show($codigo_documento)
    {
        $tDoc = TipoDocumento::findOrFail($codigo_documento);
        return $tDoc;
    }

    /**
     * Actualiza un tipo de documento específico en la DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoDocumento  $tipoDocumento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tDoc = TipoDocumento::findOrFail($request->codigo_docuemento);
        $tDoc->nombre_documento = $request->nombre_docuemento;
        return $tDoc;
    }

    /**
     * Actualiza el estado del tipo de documento en la BD (soft-delete)
     *
     * @param  \App\TipoDocumento  $tipoDocumento
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_documento)
    {
        $tDoc = TipoDocumento::findOrFail($codigo_documento);
        $tDoc->estado_documento = !$tDoc->estado_documento;
    }
}
