<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Muestra los todos los usuraios que pertenecen a la empresa requerida
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_empresa)
    {
        $usuarios = User::join('roles', 'users.user_role', '=', 'roles.codigo_rol')
            ->join('tipo_documentos', 'users.document_type', '=', 'tipo_documentos.codigo_documento')
            ->select(
                'users.user_id',
                'users.name',
                'users.lastname',
                'users.email',
                'users.user_phone',
                'users.document_type',
                'roles.codigo_rol',
                'roles.nombre_rol',
                'tipo_documentos.nombre_docuemnto',
                'users.empresa_usuario',
                'users.user_state'
            )
            ->where('users.empresa_usuario', '=', $id_empresa)
            ->orderBy('users.user_id', 'asc')->get();
        return $usuarios;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_empresa, Request $request)
    {
        $usuario = new User();
        $usuario->user_id = $request->user_id;
        $usuario->name = $request->name;
        $usuario->lastname = $request->lastname;
        $usuario->email = $request->email;
        $usuario->password = Hash::make($request->password);
        $usuario->user_phone = $request->user_phone;
        $usuario->user_role = $request->user_role;
        $usuario->document_type = $request->document_type;
        $usuario->empresa_usuario = $id_empresa;
        $usuario->save();
        return $usuario;
    }

    /**
     * Busca un usuario en determinada empresa
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_empresa, $id_usuario)
    {
        $usuario = User::join('roles', 'users.user_role', '=', 'roles.codigo_rol')
            ->join('tipo_documentos', 'users.document_type', '=', 'tipo_documentos.codigo_documento')
            ->select(
                'users.user_id',
                'users.name',
                'users.lastname',
                'users.email',
                'users.user_phone',
                'users.document_type',
                'roles.codigo_rol',
                'roles.nombre_rol',
                'tipo_documentos.nombre_docuemnto',
                'users.empresa_usuario'
            )
            ->where('users.empresa_usuario', '=', $id_empresa)
            ->findOrFail($id_usuario);
        return $usuario;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id_empresa, Request $request)
    {
        $usuario = User::findOrFail($request->user_id);
        //Validamos que el usuario a editar si pertenezca a la empresa solicitada
        if ($usuario->empresa_usuario != $id_empresa)
            return "El usuario no esxiste en esta empresa";
        else {
            $usuario->name = $request->name;
            $usuario->lastname = $request->lastname;
            $usuario->email = $request->email;
            $usuario->password = $request->password;
            $usuario->user_phone = $request->user_phone;
            $usuario->user_role = $request->user_role;
            $usuario->document_type = $request->document_type;

            $usuario->save();
            return $usuario;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_empresa, $id_usuario)
    {
        $usuario = User::findOrFail($id_usuario);
        //Validamos que el usuario a editar si pertenezca a la empresa solicitada
        if ($usuario->empresa_usuario != $id_empresa)
            return "El usuario no esxiste en esta empresa";
        else {
            $usuario->user_state = !$usuario->user_state;
            $usuario->save();
            return $usuario;
        }
    }
    /**
     * Busca por parametro, se debe enviar el criterio con el valor de la columna donde buscar y en dato a buscar
     */
    public function buscar($criterio, $busqueda)
    {
        $usuario = User::join('roles', 'users.user_role', '=', 'roles.codigo_rol')
            ->join('tipo_documentos', 'users.document_type', '=', 'tipo_documentos.codigo_documento')
            ->select(
                'users.user_id',
                'users.name',
                'users.lastname',
                'users.email',
                'users.user_phone',
                'users.document_type',
                'roles.codigo_rol',
                'roles.nombre_rol',
                'tipo_documentos.nombre_docuemnto'
            )->where('users.' . $criterio, 'like', '%' . $busqueda . '%')->get();
        return $usuario;
    }

    /**
     * obtener los usuarios y el nombre y id de la sucursal
     */
    public function obtenerSucursal()
    {
        $usuario = User::join('sucursales', 'productos.sucursal_producto', '=', 'sucursales.id_sucursal')
            ->select(
                'users.user_id',
                'users.name',
                'users.lastname',
                'users.email',
                'users.user_phone',
                'users.document_type',
                'roles.codigo_rol',
                'roles.nombre_rol',
                'sucursales.id_sucursal',
                'sucursales.nombre_sucursal',
                'sucursales.nit_sucursal'
            )->get();

        return $usuario;
    }

    /**
     * obtener los usuarios y el nombre y id de la empresa
     */
    public function obtenerEmpresa()
    {
        $usuario = User::join('roles', 'users.user_role', '=', 'roles.codigo_rol')
            ->join('users_empresas', 'users.user_id', '=', 'users_sucursales.user_id_UE')
            ->join('empresas', 'users_empresas.codigo_empresa_UE', '=', 'empresas.id_empresa')
            ->select(
                'users.user_id',
                'users.name',
                'users.lastname',
                'users.email',
                'users.user_phone',
                'users.document_type',
                'roles.codigo_rol',
                'roles.nombre_rol',
                'empresas.id_empresa',
                'empresas.nombre_empresa',
                'empresas.nit_empresa'
            )->get();

        return $usuario;
    }
    public function getSucursal($id_usuario){
        $usuarios = User::join('users_sucursales', 'users.user_id', '=', 'users_sucursales.user_id_US')
            ->join('sucursales','users_sucursales.codigo_sucursal_US','=','sucursales.id_sucursal')
            ->select(
                'users.user_id',
                'users.user_role',
                'users.user_state',
                'sucursales.id_sucursal'
            )
            ->where('user_id', '=', $id_usuario);
        return $usuarios;
    }
    
}
