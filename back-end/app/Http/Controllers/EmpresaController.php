<?php

namespace App\Http\Controllers;

use App\Empresa;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    /**
     * Devuelve todas las empresas registradas en la base de datos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::all();
        if ($empresas)
        return $empresas;
        return "No hay registros en la base de datos";
    }

    /**
     * Almacena una empresa en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $empresa = new Empresa();
            $empresa->nombre_empresa = $request->nombre_empresa;
            $empresa->nit_empresa = $request->nit_empresa;
            $empresa->save();
            return $empresa;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Muesta la empresa seleccionada medianta su código
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        try {
            $temp = Empresa::findOrFail($empresa->id_empresa);
            return $temp;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Actualiza los datos de la empresa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $temp = Empresa::findOrFail($request->id_empresa);
            $temp->nit_empresa = $request->nit_empresa;
            $temp->nombre_empresa = $request->nombre_empresa;
            $temp->save();
            return $temp;
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Rrealiza eliminación lógica de la empresa registrada, es decir, su estado para de verdadero a falso, pero no se elimina el registro
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_empresa)
    {
        try {
            $temp = Empresa::findOrFail($id_empresa);
            $temp->estado_empresa = !$temp->estado_empresa;
            $temp->save();
            return $temp;
        } catch (Exception $e) {
            return $e;
        }
    }
    /**
     * 
     */
    public function buscar($criterio,$busqueda)
    {
        return DB::table('empresas')->where($criterio,'like','%'.$busqueda.'%')->get();
         
    }
}
