<?php

namespace App\Http\Controllers;

use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Muestra todos los roles almacenados en la BD
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::all();
        return $roles;
    }


    /**
     * Almacena un nuevo rol en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol = new Rol();
        $rol->nombre_rol = $request->nombre_rol;
        return $rol;
    }

    /**
     * Muestra un rol almacenado en la BD
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Rol $rol)
    {
        $temp = Rol::findOrFail($rol->codigo_rol);
        return $temp;
    }

    /**
     * Actualiza el rol especificado en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $temp = new Rol();
        $temp->nombre_rol = $request->nombre_rol;
        return $temp;
    }

    /**
     * Actualiza el rol especifico almacenado en la BD, modificando su estado (Soft-delete)
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy($codigo_rol)
    {
        $temp = Rol::findOrFail($codigo_rol);
        $temp->estado_rol = !$temp->estado_rol;
    }
}
