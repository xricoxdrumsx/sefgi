<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersEmpresa extends Model
{
    //Declaramos los campos que pueden ser editados
    protected $fillable = ['codigo_empresa_UE', 'user_id_UE'];
}
