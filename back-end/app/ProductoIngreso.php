<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoIngreso extends Model
{

    //Asignamos los campos que pueden ser modificados
    protected $fillable = [
        'producto_ingreso_PI',
        'ingreso_producto_PI',
        'stock_productos_i'
    ];

    
}
