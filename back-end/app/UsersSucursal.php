<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSucursal extends Model
{
    //Referenciamos nuesta llave primaria
    protected $table = 'users_sucursales';

    //Definimos los campos que pueden ser modificados
    protected $fillable = [
        'codigo_sucursal_US',
        'user_id_US',
    ];
}
