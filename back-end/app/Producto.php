<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //Asignamos la llave primaria personalizada
    protected $primaryKey = 'codigo_producto';

    //Definimos los atributos que pueden ser llenados en la base de datos
    protected $fillabe = [
        'nombre_producto', 'descripcion_producto', 'stock_producto', 'IVA',
        'precio_compra', 'precio_venta', 'sucursal_producto', 'estado_producto'
    ];

    /**
     * ____________________________________________
     *      Relaciones de Eloquent
     * ____________________________________________
     */

    public function empresa(){
        return $this->belongsTo('App\Empresa');
    }

    public function sucursal(){
        return $this->belongsTo('App\Sucursal');
    }
}
